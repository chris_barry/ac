package im.barry.ac;

import im.barry.ac.model.ACConfig;
import im.barry.ac.model.ReadingEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class DB {
    private final static Logger logger = LoggerFactory.getLogger(DB.class);

    public static String getDatabaseJDBC() {
        return "jdbc:sqlite:/usr/local/var/ac.db";
    }

    public static void databaseSetUp() {
        try(Connection conn = DriverManager.getConnection(getDatabaseJDBC())) {
            Statement stmt = conn.createStatement();

            logger.info("Creating tables");
            stmt.execute("CREATE TABLE IF NOT EXISTS temperatures ("
                    + "	id integer PRIMARY KEY,"
                    + " time DATETIME DEFAULT CURRENT_TIMESTAMP,"
                    + "	temperature INTEGER,"
                    + "	humidity INTEGER"
                    + ");");

            stmt.execute("CREATE TABLE IF NOT EXISTS config_nest ("
                    + "	id integer PRIMARY KEY,"
                    + "	access_token TEXT,"
                    + "	refresh_token TEXT,"
                    + "	client_id TEXT,"
                    + "	client_secret TEXT,"
                    + " nest_id TEXT"
                    + ");");

            stmt.execute("CREATE TABLE IF NOT EXISTS config_hab ("
                    + "	id integer PRIMARY KEY,"
                    + " token TEXT"
                    + " )");
        } catch (SQLException e) {
            logger.error("oops", e);
        }
    }

    public static ACConfig getAccessToken() {
        logger.info("Get access token");

        String accessToken = "";
        String refreshToken = "";
        String clientId = "";
        String clientSecret = "";
        String nestId = "";
        String habToken = "";

        try(Connection conn = DriverManager.getConnection(getDatabaseJDBC())) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from config_nest where id=1");

            while(rs.next()) {
                accessToken = rs.getString("access_token");
                refreshToken = rs.getString("refresh_token");
                clientId = rs.getString("client_id");
                clientSecret = rs.getString("client_secret");
                nestId = rs.getString("nest_id");
            }

            rs = stmt.executeQuery("select * from config_hab where id=1");
            while(rs.next()) {
                habToken = rs.getString("token");
            }

        }  catch (SQLException e) {
            logger.error("Error selecting access token", e);
        }
        return new ACConfig(accessToken, refreshToken, clientId, clientSecret, habToken, nestId);
    }

    public static void setTokens(final String access) {
        try(Connection conn = DriverManager.getConnection(getDatabaseJDBC())) {
            logger.info("Updating access_token");
            PreparedStatement stmt = conn.prepareStatement("UPDATE config_nest SET access_token=? WHERE id=1");
            stmt.setString(1, access);
            stmt.executeUpdate();
        }  catch (SQLException e) {
            logger.error("Error setting token", e);
        }
    }

    public static void writeReading(final ReadingEntry readingEntry) {
        try(Connection conn = DriverManager.getConnection(getDatabaseJDBC())) {
            logger.info("Adding entry with temperature: {}; humidity: {}", readingEntry.getTemperature(), readingEntry.getHumidity());
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO temperatures (temperature, humidity) values (?, ?);");
            stmt.setInt(1, readingEntry.getTemperature());
            stmt.setInt(2, readingEntry.getHumidity());
            stmt.executeUpdate();
        }  catch (SQLException e) {
            logger.error("Error updating reading", e);
        }
    }
}
