package im.barry.ac;

import im.barry.ac.model.ACConfig;
import im.barry.ac.model.ReadingEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {
    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] main) throws InterruptedException, IOException {
        ACConfig acConfig = new ACConfig("", "", "", "", "", "");
        try {
            DB.databaseSetUp();
        } catch (Exception e) {
            logger.error("Some database error");
        }

        // Try to do an http call to get Nest status 5 times.
        for(int i = 0; i <= 2; i++) {
            try {
                acConfig = DB.getAccessToken();
                ReadingEntry re = HTTP.nestStatus(acConfig);
                DB.writeReading(re);
                boolean status = re.getTemperature() >= 25 || re.getHumidity() >= 50;
                HTTP.toggleAC("ChrisAshleyAC_Switch", status, acConfig);
                TimeUnit.SECONDS.sleep(5);
                HTTP.toggleAC("LivingRoomAC_Switch", status, acConfig);
            } catch (IllegalAccessException e) {
                String token = HTTP.nestRefresh(acConfig);
                DB.setTokens(token);
                TimeUnit.SECONDS.sleep(i);
                continue;
            } catch (IllegalStateException e) {
                logger.info("unknown error");
                break;
            }
            break;
        }
    }

}
