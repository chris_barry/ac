package im.barry.ac.model.nest;

public class Humidity {
    private String ambientHumidityPercent;

    public String getAmbientHumidityPercent() {
        return ambientHumidityPercent;
    }

    public void setAmbientHumidityPercent(String ambientHumidityPercent) {
        this.ambientHumidityPercent = ambientHumidityPercent;
    }
}
