package im.barry.ac.model.nest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Traits {
    @JsonProperty("sdm.devices.traits.Info")
    private Info info;

    @JsonProperty("sdm.devices.traits.Humidity")
    private Humidity humidity;

    @JsonProperty("sdm.devices.traits.Temperature")
    private Temperature temperature;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public Humidity getHumidity() {
        return humidity;
    }

    public void setHumidity(Humidity humidity) {
        this.humidity = humidity;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }


    /*
        "traits": {
        "sdm.devices.traits.Info": {
            "customName": ""
        },
        "sdm.devices.traits.Humidity": {
            "ambientHumidityPercent": 46
        },
        "sdm.devices.traits.Connectivity": {
            "status": "ONLINE"
        },
        "sdm.devices.traits.Fan": {},
        "sdm.devices.traits.ThermostatMode": {
            "mode": "OFF",
                    "availableModes": [
            "HEAT",
                    "OFF"
          ]
        },
        "sdm.devices.traits.ThermostatEco": {
            "availableModes": [
            "OFF",
                    "MANUAL_ECO"
          ],
            "mode": "OFF",
                    "heatCelsius": 11.246765,
                    "coolCelsius": 24.444443
        },
        "sdm.devices.traits.ThermostatHvac": {
            "status": "OFF"
        },
        "sdm.devices.traits.Settings": {
            "temperatureScale": "CELSIUS"
        },
        "sdm.devices.traits.ThermostatTemperatureSetpoint": {},
        "sdm.devices.traits.Temperature": {
            "ambientTemperatureCelsius": 21.779999
        }

     */
}
