package im.barry.ac.model.nest;

import java.util.List;

public class Nests {
    private List<Nest> devices;

    public List<Nest> getDevices() {
        return devices;
    }

    public void setDevices(List<Nest> devices) {
        this.devices = devices;
    }
}
