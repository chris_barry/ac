package im.barry.ac.model.nest;

public class Temperature {
    private int ambientTemperatureCelsius;

    public int getAmbientTemperatureCelsius() {
        return ambientTemperatureCelsius;
    }

    public void setAmbientTemperatureCelsius(int ambientTemperatureCelsius) {
        this.ambientTemperatureCelsius = ambientTemperatureCelsius;
    }
}
