package im.barry.ac.model;

public class ACConfig {
    private String nestAccessToken;
    private String nestRefreshToken;
    private String nestClientId;
    private String nestClientSecret;
    private String nestId;
    private String habToken;

    public ACConfig(final String nestAccessToken, final String nestRefreshToken, final String nestClientId, final String nestClientSecret, final String habToken, final String nestId) {
        this.nestAccessToken = nestAccessToken;
        this.nestRefreshToken = nestRefreshToken;
        this.nestClientId = nestClientId;
        this.nestClientSecret = nestClientSecret;
        this.habToken = habToken;
        this.nestId = nestId;
    }

    public String getNestAccessToken() {
        return nestAccessToken;
    }

    public void setNestAccessToken(String nestAccessToken) { this.nestAccessToken = nestAccessToken; }

    public String getNestRefreshToken() {
        return nestRefreshToken;
    }

    public void setNestRefreshToken(String nestRefreshToken) {
        this.nestRefreshToken = nestRefreshToken;
    }

    public String getNestClientId() {
        return nestClientId;
    }

    public void setNestClientId(String nestClientId) {
        this.nestClientId = nestClientId;
    }

    public String getNestClientSecret() {
        return nestClientSecret;
    }

    public void setNestClientSecret(String nestClientSecret) {
        this.nestClientSecret = nestClientSecret;
    }

    public String getHabToken() { return habToken; }

    public void setHabToken(String habToken) { this.habToken = habToken; }

    public String getNestId() { return nestId; }

    public void setNestId(String nestId) { this.nestId = nestId; }

}
