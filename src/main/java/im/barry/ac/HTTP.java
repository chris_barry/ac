package im.barry.ac;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import im.barry.ac.model.ACConfig;
import im.barry.ac.model.ReadingEntry;
import im.barry.ac.model.RefreshTokenModel;
import im.barry.ac.model.nest.Nests;
import im.barry.ac.model.nest.Traits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class HTTP {
    private final static Logger logger = LoggerFactory.getLogger(HTTP.class);
    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static HttpClient getHttpClient() {
        logger.debug("Create HTTP Client");
        return HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .connectTimeout(Duration.ofSeconds(5))
                .build();
    }

    public static void toggleAC(final String item, final boolean status, final ACConfig acConfig) throws IOException, InterruptedException {
        String s = status ? "ON" : "OFF";
        logger.info("Toggling air conditioner to {}", s);

        String uri = String.format("http://192.168.2.89:8080/rest/items/%s", item);
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(s))
                .uri(URI.create(uri))
                .timeout(Duration.ofSeconds(5))
                .header("Content-Type", "text/plain")
                .header("Authorization", "Bearer " + acConfig.getHabToken())
                .build();

        HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        logger.info("response status code: {}", response.statusCode());
        logger.info("response body: {}", response.body());
    }

    public static ReadingEntry nestStatus(final ACConfig acConfig) throws IOException, InterruptedException, IllegalAccessException {
        logger.info("Call Nest API");

        String uri = String.format("https://smartdevicemanagement.googleapis.com/v1/enterprises/%s/devices", acConfig.getNestId());
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .timeout(Duration.ofSeconds(5))
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + acConfig.getNestAccessToken())
                .build();

        HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        if(response.statusCode() == 200) {
            try {
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                Nests s = objectMapper.readValue(response.body(), Nests.class);
                Traits t = s.getDevices().get(0).getTraits();
                logger.info("Temp: {}; Humidity: {}", t.getTemperature().getAmbientTemperatureCelsius(), t.getHumidity().getAmbientHumidityPercent());
                return new ReadingEntry(Integer.parseInt(t.getHumidity().getAmbientHumidityPercent()), t.getTemperature().getAmbientTemperatureCelsius());
            } catch (Exception e) {
                logger.error("Issue deserializing");
                throw new IllegalStateException();
            }
        } else if (response.statusCode() == 401) {
            logger.error("Token error, refresh required");
            throw new IllegalAccessException();
        }
        return new ReadingEntry(0, 0);
    }

    public static String nestRefresh(final ACConfig acConfig) throws IOException, InterruptedException {
        logger.info("Fetch new bearer token");

        String uri = String.format("https://www.googleapis.com/oauth2/v4/token?client_id=%s&client_secret=%s&refresh_token=%s&grant_type=refresh_token", acConfig.getNestClientId(), acConfig.getNestClientSecret(), acConfig.getNestRefreshToken());
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.noBody())
                .uri(URI.create(uri))
                .timeout(Duration.ofSeconds(5))
                .build();

        HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        RefreshTokenModel s = objectMapper.readValue(response.body(), RefreshTokenModel.class);

        return s.getAccessToken();
    }
}
